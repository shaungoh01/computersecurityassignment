@extends("layouts.app")

@section("main")
    <h1 class="cover-heading">Symmetric Cipher -  AES</h1>
    <h1 class="cover-heading">Modern Asymmetric - RSA</h1>
    <p class="lead">RSA is one of the first public-key cryptosystems and is widely used for secure data transmission. In such a cryptosystem, the encryption key is public and it is different from the decryption key which is kept secret.</p>
<ul class="nav nav-tabs">
  <li class="nav-item">
    <a class="nav-link active" data-toggle="tab" href="#aes-en">AES Encryption</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" data-toggle="tab" href="#aes-de">AES Decryption</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" data-toggle="tab" href="#rsa-en">RSA (Coming Soon)</a>
  </li>
</ul>

<!-- Tab panes -->
<div class="tab-content" style="margin-top:20px;">
  <div class="tab-pane container active" id="aes-en">
        <!-- <div class="row">
        </div> -->
        <div class="row">
            <label for="encrypt" class="center col-md-5">Encrypt</label>
            <label for="key" class="center col-md-5 offset-2">Key</label>
        </div>
        <div class="row">
            <input id="encrypt" class="form-control col-md-5" type="string" name="encrypt">
            <input id="key" class="form-control col-md-5 offset-2" type="string" name="key">
        </div>
        <br>
        <div class="row">
            <label for="decrypt" class="center col-md-3 offset-1">Decrypt</label>
            <input id="decrypt" class="form-control col-md-7 offset-1" type="string" name="decrypt">
        </div>
        <div id="time-div" class="center hide" style="margin-top:20px;">
        <p id="process-time"></p>
        </div>
        <div class="center" style="margin-top:20px;">
        <btn type="btn" id="aes-encrypt-btn" class="btn btn-lg btn-secondary">Encrypt (Plain Text)</btn>
        <btn type="btn" id="aes-encrypt-btn-hex" class="btn btn-lg btn-secondary">Encrypt (Hex Text)</btn>
        </div>
    </div>
  <div class="tab-pane container fade" id="aes-de">..23.</div>
  <div class="tab-pane container fade" id="rsa-en">
            <div class="row">
                <p class="center col-md-6">Person A public Key: <span id="person-a-public"></span></p>
                <p class="center col-md-6">Person A private Key: <span id="person-a-private"></span></p>
                <p class="center col-md-6">Person B public Key: <span id="person-b-public"></span></p>
                <p class="center col-md-6">Person B private Key: <span id="person-b-private"></span></p>
            </div>
            <div class="row">
                <label for="encrypt" class="center col-md-4">Encrypt</label>
                <label for="key" class="center col-md-2 offset-1">Key</label>
                <label for="decrypt" class="center col-md-4 offset-1">Decrypt</label>
            </div>
            <div class="row">
                <input id="encrypt" class="form-control col-md-4" type="string" name="encrypt">
                <input id="key" class="form-control col-md-2 offset-1" type="number" name="key">
                <input id="decrypt" class="form-control col-md-4 offset-1" type="string" name="decrypt">
            </div>
            <div id="time-div" class="center hide" style="margin-top:20px;">
            <p id="process-time"></p>
            </div>
            <div class="center" style="margin-top:20px;">
            <btn type="btn" id="encrypt-btn" class="btn btn-lg btn-secondary">Encrypt</btn>
            <btn type="btn" id="decrypt-btn" class="btn btn-lg btn-secondary">Decrypt</btn>
            </div>
    </div>
</div>
@endsection

@section("js")
<script>
$("#aes-encrypt-btn").click(function(){
    var form = new FormData();
    form.append("encrypt", $("#encrypt").val());
    form.append("key", $("#key").val());

    var settings = {
    "async": true,
    "crossDomain": true,
    "url": "{{env('APP_URL',"http://localhost:8080")}}/api/taskb1/encrypt",
    "method": "POST",
    "processData": false,
    "contentType": false,
    "mimeType": "multipart/form-data",
    "data": form
    }

    $.ajax(settings).done(function (response) {
    console.log(JSON.parse(response).result);
    var responseObj = JSON.parse(response);
    $("#decrypt").val(responseObj.result);
    $("#process-time").text(responseObj.time);
    $("#time-div").show();
    });
})

$("#aes-encrypt-btn-hex").click(function(){
    var form = new FormData();
    form.append("encrypt", $("#encrypt").val());
    form.append("key", $("#key").val());
    form.append("hex", true);

    var settings = {
    "async": true,
    "crossDomain": true,
    "url": "{{env('APP_URL',"http://localhost:8080")}}/api/taskb1/encrypt",
    "method": "POST",
    "processData": false,
    "contentType": false,
    "mimeType": "multipart/form-data",
    "data": form
    }

    $.ajax(settings).done(function (response) {
    console.log(JSON.parse(response).result);
    var responseObj = JSON.parse(response);
    $("#decrypt").val(responseObj.result);
    $("#process-time").text(responseObj.time);
    $("#time-div").show();
    });
})

$("#decrypt-btn").click(function(){
    var form = new FormData();
    form.append("decrypt", $("#decrypt").val());
    form.append("key", $("#key").val());

    var settings = {
    "async": true,
    "crossDomain": true,
    "url": "{{env('APP_URL',"http://localhost:8080")}}/api/taskb1/decrypt",
    "method": "POST",
    "processData": false,
    "contentType": false,
    "mimeType": "multipart/form-data",
    "data": form
    }

    $.ajax(settings).done(function (response) {
    console.log(JSON.parse(response).result);
    var responseObj = JSON.parse(response);
    $("#encrypt").val(responseObj.result);
    $("#process-time").text(responseObj.time);
    $("#time-div").show();
    });
})
</script>
@endsection