<!doctype html>
<html lang="{{ app()->getLocale() }}">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../../../favicon.ico">

    <title>Computer Security Assignment</title>

    <!-- Bootstrap core CSS -->
    <!-- <link href="../../../../dist/css/bootstrap.min.css" rel="stylesheet"> -->
    <link href="/css/app.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="/css/cover.css" rel="stylesheet">
  </head>

  <body class="text-center">

    <div class="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">
      <header class="masthead mb-auto">
        <div class="inner">
          <h3 class="masthead-brand">Computer Security Assignment</h3>
          <nav class="nav nav-masthead justify-content-center">
            <a class="nav-link {{\Request::is('/')?"active":"" }}" href="/">Home</a>
            <a class="nav-link {{\Request::is('taska1*')?"active":"" }}" href="/taska1">Part A-1</a>
            <a class="nav-link {{\Request::is('taska2*')?"active":"" }}" href="/taska2">Part A-2</a>
            <a class="nav-link {{\Request::is('taska3*')?"active":"" }}" href="/taska3">Part A-3</a>
            <a class="nav-link {{\Request::is('taskb1*')?"active":"" }}" href="/taskb1">Part B</a>
          </nav>
        </div>
      </header>

      <main role="main" class="inner cover">
        @yield("main")
      </main>

      <footer class="mastfoot mt-auto">
        <div class="inner">
          <p>With the help form <a href="https://getbootstrap.com/">Bootstrap</a> &amp; <a href="https://laravel.com/docs">Laravel</a>.</p>
        </div>
      </footer>
    </div>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <!-- <script>window.jQuery || document.write('<script src="../../../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="../../../../assets/js/vendor/popper.min.js"></script> -->
    <script src="/js/app.js"></script>
    @yield("js")
  </body>
</html>
