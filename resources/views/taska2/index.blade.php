@extends("layouts.app")

@section("main")
    <h1 class="cover-heading">Polyalphabetic Substitution Ciphers - Auto Key Cipher</h1>
    <p class="lead">An autokey cipher is a polyalphabetic substitution cipher that incorporates the message into the key. The key is generated from the message in an automated fashion.</p>

        <form>
        <div class="row">
            <label for="encrypt" class="center col-md-4">Encrypt</label>
            <label for="key" class="center col-md-2 offset-1">Key</label>
            <label for="decrypt" class="center col-md-4 offset-1">Decrypt</label>
        </div>
        <div class="row">
            <input id="encrypt" class="form-control col-md-4" type="string" name="encrypt">
            <input id="key" class="form-control col-md-2 offset-1" type="number" name="key">
            <input id="decrypt" class="form-control col-md-4 offset-1" type="string" name="decrypt">
        </div>
        <div id="time-div" class="center hide" style="margin-top:20px;">
        <p id="process-time"></p>
        </div>
        <div class="center" style="margin-top:20px;">
        <btn type="btn" id="encrypt-btn" class="btn btn-lg btn-secondary">Encrypt</btn>
        <btn type="btn" id="decrypt-btn" class="btn btn-lg btn-secondary">Decrypt</btn>
        </div>
        </form>
    </p>
@endsection

@section("js")
<script>
$("#encrypt-btn").click(function(){
    var form = new FormData();
    form.append("encrypt", $("#encrypt").val());
    form.append("key", $("#key").val());

    var settings = {
    "async": true,
    "crossDomain": true,
    "url": "{{env('APP_URL',"http://localhost:8080")}}/api/taska2/encrypt",
    "method": "POST",
    "processData": false,
    "contentType": false,
    "mimeType": "multipart/form-data",
    "data": form
    }

    $.ajax(settings).done(function (response) {
    console.log(JSON.parse(response).result);
    var responseObj = JSON.parse(response);
    $("#decrypt").val(responseObj.result);
    $("#process-time").text(responseObj.time);
    $("#time-div").show();
    });
})

$("#decrypt-btn").click(function(){
    var form = new FormData();
    form.append("decrypt", $("#decrypt").val());
    form.append("key", $("#key").val());

    var settings = {
    "async": true,
    "crossDomain": true,
    "url": "{{env('APP_URL',"http://localhost:8080")}}/api/taska2/decrypt",
    "method": "POST",
    "processData": false,
    "contentType": false,
    "mimeType": "multipart/form-data",
    "data": form
    }

    $.ajax(settings).done(function (response) {
    console.log(JSON.parse(response).result);
    var responseObj = JSON.parse(response);
    $("#encrypt").val(responseObj.result);
    $("#process-time").text(responseObj.time);
    $("#time-div").show();
    });
})
</script>
@endsection