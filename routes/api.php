<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('taska1/encrypt','TaskA1Controller@encrypt');
Route::post('taska1/decrypt','TaskA1Controller@decrypt');
Route::post('taska2/encrypt','TaskA2Controller@encrypt');
Route::post('taska2/decrypt','TaskA2Controller@decrypt');
Route::post('taska3/encrypt','TaskA3Controller@encrypt');
Route::post('taska3/decrypt','TaskA3Controller@decrypt');
Route::post('taskb1/encrypt','TaskB1Controller@encrypt');
Route::post('taskb1/decrypt','TaskB1Controller@decrypt');