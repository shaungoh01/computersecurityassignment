<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TaskA3Controller extends Controller
{
    //
    public function index()
    {
        return view("taska3.index");
    }

    public function encrypt(Request $request)
    {
        $time_start = microtime(true);
        $cyphertext = "";
        $key = $request->key;
        $pText = $request->encrypt;//plainText
        $pTextArr = str_split($request->encrypt);
        for($i = 0; $i<$key; $i++){ //foreach row
            $evenOddCounter = 0;
            $ansKey = $i;//starting of the row
            $lastAnsKey = -1;// to check if it is the same as last one. mean no change so no need add into array
            while($ansKey < strlen($pText)){ 

                if($lastAnsKey != $ansKey){
                    $cyphertext .= $pText[$ansKey];// if its not the same as the last array, add into it
                }
                $lastAnsKey = $ansKey;

                if($evenOddCounter == 0){ //if is even run the following formula 
                    $ansKey+= 2*($key-1-$i);
                    $evenOddCounter++;
                }else{ //if is odd run the following formula 
                    $ansKey+= 2*($key-1-($key-1-$i));
                    $evenOddCounter--;
                }
            }
        }
        $timeT = "Encryption process took ". number_format(microtime(true) - $time_start, 10). " seconds.";
        return json_encode(["result"=>$cyphertext,"time"=>$timeT]);
    }

    public function decrypt(Request $request)
    {
        $time_start = microtime(true);
        $key = $request->key;
        $cText = $request->decrypt;//plainText
        $cTextArr = str_split($request->decrypt);
        $pText = array();
        $pTextTarget = 0;
        for($i = 0; $i<$key; $i++){
        $evenOddCounter=0;
        $ansKey = $i;
        $lastAnsKey = -1;
            while($ansKey < strlen($cText)){
                if($lastAnsKey != $ansKey){
                    $pText[$ansKey] = $cTextArr[$pTextTarget]; //the main different over encrytion is here. find the location of the plain text.
                    $pTextTarget++;
                }
                $lastAnsKey = $ansKey;

                if($evenOddCounter == 0){
                    $ansKey+= 2*($key-1-$i);
                    $evenOddCounter++;
                }else{
                    $ansKey+= 2*($key-1-($key-1-$i));
                    $evenOddCounter--;
                }
            }
        }
        ksort($pText);
        $pText = join($pText); 

        $timeT = "Decryption process took ". number_format(microtime(true) - $time_start, 10). " seconds.";
        return json_encode(["result"=>$pText,"time"=>$timeT]);
    }

}
