<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TaskB1Controller extends Controller
{
    public function index()
    {
        return view("taskb1.index");
    }

    public function byteSub($state)
    {
        $sBox = [
                ['63','7C','77','7b','f2','6b','6f','c5','30','01','67','2b','fe','d7','ab','76'],
                ['ca','82','c9','7d','fa','59','47','f0','ad','d4','a2','af','9c','a4','72','c0'],
                ['b7','fd','93','26','36','3f','f7','cc','34','a5','e5','f1','71','d8','31','15'],
                ['04','c7','23','c3','18','96','05','9a','07','12','80','e2','eb','27','b2','75'],
                ['09','83','2c','1a','1b','6e','5a','a0','52','3b','d6','b3','29','e3','2f','84'],
                ['53','d1','00','ed','20','fc','b1','5b','6a','cb','be','39','4a','4c','58','cf'],
                ['d0','ef','aa','fb','43','4d','33','85','45','f9','02','7f','50','3c','9f','a8'],
                ['51','a3','40','8f','92','9d','38','f5','bc','b6','da','21','10','ff','f3','d2'],
                ['cd','0c','13','ec','5f','97','44','17','c4','a7','7e','3d','64','5d','19','73'],
                ['60','81','4f','dc','22','2a','90','88','46','ee','b8','14','de','5e','0b','db'],
                ['e0','32','3a','0a','49','06','24','5c','c2','d3','ac','62','91','95','e4','79'],
                ['e7','c8','37','6d','8d','d5','4e','a9','6c','56','f4','ea','65','7a','ae','08'],
                ['ba','78','25','2e','1c','a6','b4','c6','e8','dd','74','1f','4b','bd','8b','8a'],
                ['70','3e','b5','66','48','03','f6','0e','61','35','57','b9','86','c1','1d','9e'],
                ['e1','f8','98','11','69','d9','8e','94','9b','1e','87','e9','ce','55','28','df'],
                ['8c','a1','89','0d','bf','e6','42','68','41','99','2d','0f','b0','54','bb','16'],
        ];
        foreach($state as $stateCol => $stateCols){
            foreach($stateCols as $stateRow => $val){
                $vals = str_split($val);
                $row = base_convert($vals[0], 16, 10);
                $col = base_convert($vals[1], 16, 10);
                $state[$stateCol][$stateRow] = $sBox[$row][$col];
            }
        }
        return $state;
        
    }

    public function xOr($v1,$v2){
        $returnV ="";
        foreach(str_split($v1) as $valK => $val1){
            if($val1 == $v2[$valK]){
                $returnV.='0';
            }else{
                $returnV.='1';
            }
        }
        return $returnV;
    }

    public function addRoundKey($state,$key)
    {
        foreach($state as $stateCol => $stateCols){
            foreach($stateCols as $stateRow => $val){
                $valBin = str_pad(base_convert($val, 16, 2), 16, "0", STR_PAD_LEFT);
                $keyBin = str_pad(base_convert($key[$stateCol][$stateRow], 16, 2), 16, "0", STR_PAD_LEFT);
                    // dd($keyBin);
                $returnValBin = $this->xOr($valBin,$keyBin);
                // if($stateCol == 3 && $stateRow == 3){
                //     dd($valBin);
                // }
                // $state[$stateCol][$stateRow]=$returnValBin;
                $state[$stateCol][$stateRow]=str_pad(base_convert($returnValBin, 2, 16), 2, "0", STR_PAD_LEFT);
                // $state[$stateCol][$stateRow] = pack('h*', base_convert($returnValBin, 2, 16));
            }
        }
        return $state;
    }

    public function shiftRows($state)
    {
        foreach($state as $stateCol => $stateCols){
            foreach($stateCols as $stateRow => $val){
                $move = ($stateCol-$stateRow)%4;
                if($move<0){
                    $move+=4;
                }
                $returnState[$move][$stateRow] = $val;
            }
        }
        return $returnState;
    }

    public function mixColumns($state,$debug=false)
    {
        $mixCol = [
                    [2,3,1,1],
                    [1,2,3,1],
                    [1,1,2,3],
                    [3,1,1,2],
        ];
        
        foreach($state as $stateCol => $stateCols){

            foreach($stateCols as $stateRow => $val){
                foreach($mixCol[$stateRow] as $i=>$mixColCol){
                    if($mixColCol == 1){
                        $result[$i] = str_pad(base_convert($stateCols[$i], 16, 2), 8, "0", STR_PAD_LEFT);
                    }else if($mixColCol == 2){
                        $original = str_pad(base_convert($stateCols[$i], 16, 2), 8, "0", STR_PAD_LEFT);

                        $msb = substr($original,0,1);
                        $result[$i] = substr($original,1,7)."0";
                        if($msb == "1"){
                            $result[$i] = $this->xOr($result[$i],"00011011");
                        }
                    }else{

                        $original = str_pad(base_convert($stateCols[$i], 16, 2), 8, "0", STR_PAD_LEFT);
                        if($debug && $i ==1){
                            dd( $stateCols[$i]);
                        }
                        $msb = substr($original,0,1);
                        $original2 = substr($original,1,7)."0";
                        if($msb == "1"){
                            $original2 = $this->xOr($original2,"00011011");
                        }
                        $result[$i] = $this->xOr($original,$original2);
                    }
                }

                // dd(str_pad(base_convert($result[1], 2, 16) , 2, "0", STR_PAD_LEFT));
                $finalNumber = $this->xOr($this->xOr($this->xOr($result[0],$result[1]) ,$result[2] ) , $result[3]);
                $returnState[$stateCol][$stateRow] = str_pad(base_convert($finalNumber, 2, 16) , 2, "0", STR_PAD_LEFT);
                if($debug){
                    dd( $result);
                }
                // dd($finalNumber);
                // dd(str_pad(base_convert($finalNumber, 2, 16) , 2, "0", STR_PAD_LEFT));
            }
        }
        return $returnState;
    }

    public function keyGeneration($key)
    {
        $rcon=[
            ['01','00','00','00'],
            ['02','00','00','00'],
            ['04','00','00','00'],
            ['08','00','00','00'],
            ['10','00','00','00'],
            ['20','00','00','00'],
            ['40','00','00','00'],
            ['80','00','00','00'],
            ['1b','00','00','00'],
            ['36','00','00','00'],
        ];

        $sBox = [
                ['63','7C','77','7b','f2','6b','6f','c5','30','01','67','2b','fe','d7','ab','76'],
                ['ca','82','c9','7d','fa','59','47','f0','ad','d4','a2','af','9c','a4','72','c0'],
                ['b7','fd','93','26','36','3f','f7','cc','34','a5','e5','f1','71','d8','31','15'],
                ['04','c7','23','c3','18','96','05','9a','07','12','80','e2','eb','27','b2','75'],
                ['09','83','2c','1a','1b','6e','5a','a0','52','3b','d6','b3','29','e3','2f','84'],
                ['53','d1','00','ed','20','fc','b1','5b','6a','cb','be','39','4a','4c','58','cf'],
                ['d0','ef','aa','fb','43','4d','33','85','45','f9','02','7f','50','3c','9f','a8'],
                ['51','a3','40','8f','92','9d','38','f5','bc','b6','da','21','10','ff','f3','d2'],
                ['cd','0c','13','ec','5f','97','44','17','c4','a7','7e','3d','64','5d','19','73'],
                ['60','81','4f','dc','22','2a','90','88','46','ee','b8','14','de','5e','0b','db'],
                ['e0','32','3a','0a','49','06','24','5c','c2','d3','ac','62','91','95','e4','79'],
                ['e7','c8','37','6d','8d','d5','4e','a9','6c','56','f4','ea','65','7a','ae','08'],
                ['ba','78','25','2e','1c','a6','b4','c6','e8','dd','74','1f','4b','bd','8b','8a'],
                ['70','3e','b5','66','48','03','f6','0e','61','35','57','b9','86','c1','1d','9e'],
                ['e1','f8','98','11','69','d9','8e','94','9b','1e','87','e9','ce','55','28','df'],
                ['8c','a1','89','0d','bf','e6','42','68','41','99','2d','0f','b0','54','bb','16'],
        ];

        $keyCol = $key[3];
        $keys[0] = $key; 
        for($i = 1 ; $i<11 ; $i++){
            for($i1=0; $i1<4 ; $i1++){
                if($i1 == 0){
                    $t = $keyCol[0];
                    $keyCol[0] = $keyCol[1];
                    $keyCol[1] = $keyCol[2];
                    $keyCol[2] = $keyCol[3];
                    $keyCol[3] = $t;
                
                    // dd($keyCol);
                    foreach($keyCol as $i2=>$val){
                        $vals = str_split($val);
                        $row = base_convert($vals[0], 16, 10);
                        $col = base_convert($vals[1], 16, 10);
                        $keyCol[$i2] = $sBox[$row][$col];
                        if($i2 == 1){
                            // dd($vals);
                        }

                        $keyCol[$i2] = $this->xOr(str_pad(base_convert($keyCol[$i2], 16, 2), 8, "0", STR_PAD_LEFT),str_pad(base_convert($rcon[$i-1][$i2], 16, 2), 8, "0", STR_PAD_LEFT));
                        $keyCol[$i2] = str_pad(base_convert($keyCol[$i2], 2, 16) , 2, "0", STR_PAD_LEFT);
                    }
                    // dd($keyCol);
                }else{

                }
                foreach($keyCol as $i2=>$val){
                        $keyCol[$i2] = $this->xOr(str_pad(base_convert($keyCol[$i2], 16, 2), 8, "0", STR_PAD_LEFT),str_pad(base_convert($keys[$i-1][$i1][$i2], 16, 2), 8, "0", STR_PAD_LEFT));
                        $keyCol[$i2] = str_pad(base_convert($keyCol[$i2], 2, 16) , 2, "0", STR_PAD_LEFT);
                }
                // dd($keyCol);
                $keys[$i][$i1] = $keyCol;
            }
        }
        return $keys;
    }

    public function encrypt(Request $request)
    {
        $time_start = microtime(true);
        $datas = $request->encrypt;
        $keyString = $request->key;
        if(!$request->has("hex")){
            $datas = unpack("H*",$datas)[1];
            // $keyString = unpack("H*",$keyString);
        }
        $cyphertext = "";
        foreach(str_split($datas,32) as $data){
        $data = str_pad($data, 32, "7a", STR_PAD_RIGHT);

            // print_r($value);
            $vals = str_split($data , 2);
            foreach($vals as $valKey => $val){
                $trueVal[floor($valKey / 4)][$valKey%4] = $val; //first [] as col , second [] as row
            }
            // $keyString = "2b7e151628aed2a6abf7158809cf4f3c";
            $keyStrings = str_split($keyString , 2);
            foreach($keyStrings as $valKey => $val){
                $key[floor($valKey / 4)][$valKey%4] = $val; //first [] as col , second [] as row
            }
            $keys = $this->keyGeneration($key);

            // for ($keyI = 0; $keyI < 11; $keyI++) {
            //     echo "<h2>Key ". $keyI ." </h2>";
            //     echo "<div style=\"width:100%;float:left;\">";
            //     for ($row = 0; $row < 4; $row++) {
            //         echo "<ul style=\"float:left\">";
            //         for ($col = 0; $col < 4; $col++) {
            //             echo "<li>".$keys[$keyI][$row][$col]."</li>";
            //         }
            //         echo "</ul>";
            //     }
            //     echo"</div>";
            // }

            $stateAdd = $this->addRoundKey($trueVal,$keys[0]);
            // print_r($stateAdd);
            // print_r($trueVal[0]);
            for($i = 0; $i<10;$i++){
                $stateSBox = $this->byteSub($stateAdd);
                // print_r($stateSBox);

                $stateShiftRow = $this->shiftRows($stateSBox);
                if($i!= 9){
                    $stateMixColumns = $this->mixColumns($stateShiftRow);
                }else{
                    $stateMixColumns = $stateShiftRow;
                }

                $stateAdd = $this->addRoundKey($stateMixColumns,$keys[$i+1]);
            }
            for ($row = 0; $row < 4; $row++) {
                for ($col = 0; $col < 4; $col++) {
                    $cyphertext .=$stateAdd[$row][$col];
                }
            }

        }
        // $cyphertext .=" | ";
        // for ($row = 0; $row < 4; $row++) {
        //     for ($col = 0; $col < 4; $col++) {
        //         $cyphertext .= pack("H*",$stateAdd[$row][$col]);
        //     }
        // }
        // dd($cyphertext);
        $timeT = "Decryption process took ". number_format(microtime(true) - $time_start, 10). " seconds.";
        return json_encode(["result"=>$cyphertext,"time"=>$timeT]);
    }

    public function decrypt(Request $request)
    {
        $time_start = microtime(true);
        $alphas = range('a', 'z');
        $key = ($request->key)%26;
        $cyphertext = "";
        foreach(str_split($request->decrypt) as $val){
            if($val == " "){
                continue;
            }
            $num = array_search(strtolower($val),$alphas);
            $num-=$key;
            $num= $num%26;
            $key = $num;
            if ($num < 0)
            {
                $num += 26;
            }
            $cyphertext.=$alphas[$num];
        }
        $timeT = "Decryption process took ". number_format(microtime(true) - $time_start, 10). " seconds.";
        return json_encode(["result"=>$cyphertext,"time"=>$timeT]);
    }

    public function test()
    {
        $data = "PHPT";
        $value = unpack("H*",$data);
        // print_r($value);
        $value[1] ='3243f6a8885a308d313198a2e0370734';
        $vals = str_split('3243f6a8885a308d313198a2e0370734' , 2);
        foreach($vals as $valKey => $val){
            $trueVal[floor($valKey / 4)][$valKey%4] = $val; //first [] as col , second [] as row
        }
        $keyString = "01010101010101010101010101010101";
        $keyString = "2b7e151628aed2a6abf7158809cf4f3c";
        $keyStrings = str_split($keyString , 2);
        foreach($keyStrings as $valKey => $val){
            $key[floor($valKey / 4)][$valKey%4] = $val; //first [] as col , second [] as row
        }
        $keys = $this->keyGeneration($key);

        // for ($keyI = 0; $keyI < 11; $keyI++) {
        //     echo "<h2>Key ". $keyI ." </h2>";
        //     echo "<div style=\"width:100%;float:left;\">";
        //     for ($row = 0; $row < 4; $row++) {
        //         echo "<ul style=\"float:left\">";
        //         for ($col = 0; $col < 4; $col++) {
        //             echo "<li>".$keys[$keyI][$row][$col]."</li>";
        //         }
        //         echo "</ul>";
        //     }
        //     echo"</div>";
        // }

        $stateAdd = $this->addRoundKey($trueVal,$keys[0]);
        // print_r($stateAdd);
        // print_r($trueVal[0]);
        for($i = 0; $i<10;$i++){
            echo "<h2>Key ". ($i+1) ." </h2>";
            $stateSBox = $this->byteSub($stateAdd);
            // print_r($stateSBox);

            echo "<div style=\"width:25%;float:left;\">";
            for ($row = 0; $row < 4; $row++) {
                echo "<ul style=\"float:left\">";
                for ($col = 0; $col < 4; $col++) {
                    echo "<li>".$stateSBox[$row][$col]."</li>";
                }
                echo "</ul>";
            }
            echo"</div>";
            $stateShiftRow = $this->shiftRows($stateSBox);

            echo "<div style=\"width:25%;float:left;\">";
            for ($row = 0; $row < 4; $row++) {
                echo "<ul style=\"float:left\">";
                for ($col = 0; $col < 4; $col++) {
                    echo "<li>".$stateShiftRow[$row][$col]."</li>";
                }
                echo "</ul>";
            }
            echo"</div>";
            if($i!= 9){
                $stateMixColumns = $this->mixColumns($stateShiftRow);
            }else{
                $stateMixColumns = $stateShiftRow;
            }

            echo "<div style=\"width:25%;float:left;\">";
            for ($row = 0; $row < 4; $row++) {
                echo "<ul style=\"float:left\">";
                for ($col = 0; $col < 4; $col++) {
                    echo "<li>".$stateMixColumns[$row][$col]."</li>";
                }
                echo "</ul>";
            }
            echo"</div>";

            $stateAdd = $this->addRoundKey($stateMixColumns,$keys[$i+1]);
            echo "<div style=\"width:25%;float:left;\">";
            for ($row = 0; $row < 4; $row++) {
                echo "<ul style=\"float:left\">";
                for ($col = 0; $col < 4; $col++) {
                    echo "<li>".$stateAdd[$row][$col]."</li>";
                }
                echo "</ul>";
            }
            echo"</div>";
        }

        for ($row = 0; $row < 4; $row++) {
            echo "<ul style=\"float:left\">";
            for ($col = 0; $col < 4; $col++) {
                echo "<li>".$stateAdd[$row][$col]."</li>";
            }
            echo "</ul>";
        }
        for ($row = 0; $row < 4; $row++) {
            echo "<ul style=\"float:left\">";
            for ($col = 0; $col < 4; $col++) {
                echo "<li>".pack("H*",$stateAdd[$row][$col])."</li>";
            }
            echo "</ul>";
        }
        echo"<br>";
        for ($row = 0; $row < 4; $row++) {
            for ($col = 0; $col < 4; $col++) {
                echo "".pack("H*",$stateAdd[$row][$col])."";
            }
        }
        exit();
        // print_r($stateMixColumns);
        // echo pack("C*",'00','01','02','03','04','05','06','07','08','09','0A','0B','0C','0D','0E','0F');
        // echo base_convert($value[1], 16, 10);
    }
}
