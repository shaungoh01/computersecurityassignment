<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TaskA2Controller extends Controller
{
    //
    public function index()
    {
        return view("taska2.index");
    }

    public function encrypt(Request $request)
    {
        $time_start = microtime(true);
        $alphas = range('a', 'z');
        $key = ($request->key)%26;
        $cyphertext = "";
        foreach(str_split($request->encrypt) as $val){
            if($val == " "){
                continue;
            }
            $num = array_search(strtolower($val),$alphas);
            $key2 = $num;
            $num+=$key;
            $num= $num%26;
            $key = $key2; //the key also move with it , the main different from TaskA1
            $cyphertext.=$alphas[$num];
        }
        $timeT = "Encryption process took ". number_format(microtime(true) - $time_start, 10). " seconds.";
        return json_encode(["result"=>$cyphertext,"time"=>$timeT]);
    }

    public function decrypt(Request $request)
    {
        $time_start = microtime(true);
        $alphas = range('a', 'z');
        $key = ($request->key)%26;
        $cyphertext = "";
        foreach(str_split($request->decrypt) as $val){
            if($val == " "){
                continue;
            }
            $num = array_search(strtolower($val),$alphas);
            $num-=$key;
            $num= $num%26;
            $key = $num;
            if ($num < 0)
            {
                $num += 26;
            }
            $cyphertext.=$alphas[$num];
        }
        $timeT = "Decryption process took ". number_format(microtime(true) - $time_start, 10). " seconds.";
        return json_encode(["result"=>$cyphertext,"time"=>$timeT]);
    }

}
