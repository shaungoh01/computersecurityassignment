<?php

namespace App\Http\Controllers;

use App\TaskA1;
use Illuminate\Http\Request;

class TaskA1Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("taska1.index");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TaskA1  $taskA1
     * @return \Illuminate\Http\Response
     */
    public function show(TaskA1 $taskA1)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TaskA1  $taskA1
     * @return \Illuminate\Http\Response
     */
    public function edit(TaskA1 $taskA1)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TaskA1  $taskA1
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TaskA1 $taskA1)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TaskA1  $taskA1
     * @return \Illuminate\Http\Response
     */
    public function destroy(TaskA1 $taskA1)
    {
        //
    }

    public function encrypt(Request $request)
    {
        $time_start = microtime(true);
        $alphas = range('a', 'z'); //get a to z
        $key = ($request->key)%26; //mod 26
        $cyphertext = "";
        foreach(str_split($request->encrypt) as $val){ //split words
            if($val == " "){ //ignore space
                continue;
            }
            $num = array_search(strtolower($val),$alphas);
            $num+=$key;
            $num= $num%26;
            $cyphertext.=$alphas[$num]; //recombine to cyphertext
        }
        $timeT = "Encryption process took ". number_format(microtime(true) - $time_start, 10). " seconds.";
        return json_encode(["result"=>$cyphertext,"time"=>$timeT]);
    }

    public function decrypt(Request $request)
    {
        $time_start = microtime(true);
        $alphas = range('a', 'z');
        $key = ($request->key)%26; //mod 26
        $cyphertext = "";
        foreach(str_split($request->decrypt) as $val){
            if($val == " "){
                continue;
            }
            $num = array_search(strtolower($val),$alphas);
            $num-=$key;
            $num= $num%26;
            if ($num < 0) // incase of negative number
            {
                $num += 26;
            }
            $cyphertext.=$alphas[$num];
        }
        $timeT = "Decryption process took ". number_format(microtime(true) - $time_start, 10). " seconds.";
        return json_encode(["result"=>$cyphertext,"time"=>$timeT]);
    }
}
